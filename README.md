# PHP Training

PHP Training with example

## Day 1: Basics of PHP
- Basic declaration of PHP
- How to create a class
- How to create a function
- How to include a class file in a working file
- How to access a function in a working file
- How to pass values to a function
- How to return a value in a function
- How to declare an array in PHP
- How to pass values as array
- How to return a array from a function
- How to print an array in a working file

## Day 2: Array and Loop Concepts
- Array count
- For loop
- For each loop
- Sort: Basic Sort Indexed Array
- Sorting by Key
- Sorting by Values
- Reverse sorts
- if else conditions
- Switch loop

# Day 3: DB Connection and Query Operations
- Basic connection
- Fetch (fetch_assoc)
- Select single and multiple item
- WHERE & AND
- num_rows concepts
- ORDER BY
- GROUP BY
- while loop
- Return values as array
- Close the connection
- Sub queries
- How to process the DB result data in the working file

# Day 4: CRUD Operations (Create, Read, Update and Delete)
- Basic Form creation
- Browser side Validation (HTML)
- Server side Validation
- MySQL side Validation
### Create
- Insert form data in DB
- Insert with escape character
- Insert_id
### Read (Covered in Day 3)
- Display in the table
### Update
- How to do global update
- How to do only necessary update
### Delete
- Alert
- Soft Delete
- Hard Delete
