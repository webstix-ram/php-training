<?php


class learn
{

    public function getallstudents()
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Students`";
        $result = $conn->query($sql);
        if ($result->num_rows > 0 ) {
            $i=0;
            while ( $row = $result->fetch_assoc() ) {
                $st[$i] = $row;
                $i++;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }

    public function getstudents($type) // Getting the Student info
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Students` where Status='".$type."' ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0 ) {
            $i=0;
            while ( $row = $result->fetch_assoc() ) {
                $st[$i] = $row;
                $i++;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }



    public function statusinfo($type) // Getting the Status Info
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Base_Status` where Code='".$type."' ";
        $result = $conn->query($sql);
        $st='';
        if ($result->num_rows > 0 ) {
            while ( $row = $result->fetch_assoc() ) {
                $st='<span class="badge badge-pill badge-'.$row['Color'].'">'.$row['Icon'].' '.$row['Name'].'</span>';
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }

    public function stucls($sid) //Getting the Student class
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Base_Classes` where ID = (SELECT ClassID FROM `Student_Class` where StudID=".$sid." AND Stauts='A')";
        $result = $conn->query($sql);
            $st=[];
        if ($result->num_rows > 0 ) {
            while ( $row = $result->fetch_assoc() ) {
                $st= $row;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }

    public function stusec($sid) //Getting the Student class
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Base_Sec` where ID = (SELECT SecID FROM `Student_Class` where StudID=".$sid." AND Stauts='A')";
        $result = $conn->query($sql);
            $st=[];
        if ($result->num_rows > 0 ) {
            while ( $row = $result->fetch_assoc() ) {
                $st= $row;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }

    public function getclassinfo($cid) //Getting the Student class
    {
        include "db-config.php";
        $sql = "SELECT * FROM `Base_Classes` where ID = $cid";
        $result = $conn->query($sql);
            $st=[];
        if ($result->num_rows > 0 ) {
            while ( $row = $result->fetch_assoc() ) {
                $st= $row;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }

    public function getallstucount() // Getting the Student info
    {
        include "db-config.php";
        $sql = "SELECT count(StudID)as stucount, ClassID FROM `Student_Class` GROUP BY ClassID";
        $result = $conn->query($sql);
        if ($result->num_rows > 0 ) {
            $i=0;
            while ( $row = $result->fetch_assoc() ) {
                $st[$i] = $row;
                $i++;
            }
        } else {
            $st = '';
        }
        return $st;
        $conn->close();

    }





}
