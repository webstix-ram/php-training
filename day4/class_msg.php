<?php
class message
{
    function alertdanger($msgcontent)
    {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"> <strong>Alert!</strong> '.$msgcontent.' <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';

    }
    function alertsuccess($msgcontent)
    {
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Info:</strong> '.$msgcontent.' <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';

    }
}
