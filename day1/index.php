<?php

require "class_lib.php";
$ln = new learn(); //Intialization of class

    ?>
<html>
<head>
  <title>Day 1: Basics of PHP</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
  <div class="container">
<h1>Day 1: PHP Basics</h1>
<h6>By Ram | git@gitlab.com:webstix-lab/php-training.git</h6>
<hr/>
  <div class="starter-template">
    <?php //Your working file

    echo '<h3>Prinitng from a function</h3>';

    $ln->helloworld(); //Accessing the function

    echo '<hr/>';
    echo '<h3>Addition done in a function</h3>';
    $a=2; //Variable Declaration
    $b=3;
    $ln->basicadd($a, $b); //Passing the variables
    echo '<hr/>';

    echo '<h3>Return value from a function</h3>';
    $a1=20; //Variable Declaration
    $b1=30;
    $res=$ln->addret($a1, $b1); //Passing the variables
    echo $res;

    echo '<hr/>';

    echo '<h3>Array Declaration</h3>';
    echo '<h4>Method 1: Associative Array (Type 1)</h4>';
    $val=[]; //Declaring an array method 1: with specific key
    $val['first']='200';//Pushing the data into array
    $val['second']='300';

    print_r($val); //Printing the array

    echo '<hr/>';
    echo '<h4>Method 2: Associative Array (Type 2)</h4>';
    $vals = array("First"=>"2000", "Second"=>"3000"); //Declaring an array Method 2: Associative Array  (with custom name as key and values)

    echo '<pre>';
    print_r($vals); //Printing the array
    echo '</pre>'; //using pre will give full array structure

    echo '<hr/>';

    echo '<h4>Method 3: Indexed Array </h4>';
    $values = array("2000", "3000"); //Declaring an array Method 2: Indexed Array (with automatic numbers as key)

    echo '<pre>';
    print_r($values); //Printing the array
    echo '</pre>'; //using pre will give full array structure

    echo '<hr/>';

    echo '<h3>Passing associative Array to a function</h3>';
    $ln->medadd($val);//Passing the array as a single variable
    echo '<hr/>';
    echo '<h3>Passing Indexed Array to a function</h3>';
    $ln->compadd($values);//Passing the array as a single variable


    ?>
</div>
</div>
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>
